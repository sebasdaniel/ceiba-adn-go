package movies_db

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

const (
	MYSQL_MOVIES_USERNAME = "root"
	MYSQL_MOVIES_PASSWORD = "toor"
	MYSQL_MOVIES_HOST     = "127.0.0.1:3306"
	MYSQL_MOVIES_SCHEMA   = "movies"
)

var DBHandler *sql.DB

func init() {
	datasourceName := fmt.Sprintf(
		"%s:%s@tcp(%s)/%s?charset=utf8",
		MYSQL_MOVIES_USERNAME,
		MYSQL_MOVIES_PASSWORD,
		MYSQL_MOVIES_HOST,
		MYSQL_MOVIES_SCHEMA,
	)

	var err error

	DBHandler, err = sql.Open("mysql", datasourceName)

	if err != nil {
		panic(err)
	}

	if err = DBHandler.Ping(); err != nil {
		panic(err)
	}

	log.Println("Database Configured!!!")
}

// DBHandler
