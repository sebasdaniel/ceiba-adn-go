package server

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model/dto"
)

func TestMain(m *testing.M) {
	// before test
	route()

	// run tests
	exitVal := m.Run()

	// after test
	//nothing at the momment...

	// exit with the exitVal from the tests
	os.Exit(exitVal)

}

func TestListMovies(t *testing.T) {
	respW := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/peliculas", nil)
	router.ServeHTTP(respW, req)

	var respDto []dto.MovieDto
	err := json.Unmarshal(respW.Body.Bytes(), &respDto)

	assert.Nil(t, err)
	assert.Equal(t, 200, respW.Code)
	assert.Greater(t, len(respDto), 0)
}

func TestGetMovieById(t *testing.T) {
	respW := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/peliculas/1", nil)
	router.ServeHTTP(respW, req)

	var respDto dto.MovieDto
	err := json.Unmarshal(respW.Body.Bytes(), &respDto)

	assert.Nil(t, err)
	assert.Equal(t, 200, respW.Code)
	assert.Equal(t, uint(1), respDto.Id)
}

func TestCreateMovie(t *testing.T) {
	respW := httptest.NewRecorder()

	movie := dto.MovieDto{
		Title:    "Los Simpsons",
		Year:     2007,
		Genre:    "Adventure/Comedy",
		ClientId: 1,
	}
	movieB, err := json.Marshal(movie)

	assert.Nil(t, err)

	req, _ := http.NewRequest("POST", "/peliculas", bytes.NewBufferString(string(movieB)))
	router.ServeHTTP(respW, req)

	var respDto dto.MovieDto
	err = json.Unmarshal(respW.Body.Bytes(), &respDto)

	assert.Nil(t, err)
	assert.Equal(t, 201, respW.Code)
	assert.Equal(t, movie.Title, respDto.Title)
}

func TestUpdateMovie(t *testing.T) {
	respW := httptest.NewRecorder()

	movie := dto.MovieDto{
		Title:    "The Simpsons Movie",
		Year:     2007,
		Genre:    "Adventure/Comedy",
		ClientId: 1,
	}
	movieB, err := json.Marshal(movie)

	assert.Nil(t, err)

	req, _ := http.NewRequest("PUT", "/peliculas/10", bytes.NewBufferString(string(movieB)))
	router.ServeHTTP(respW, req)

	var respDto dto.MovieDto
	err = json.Unmarshal(respW.Body.Bytes(), &respDto)

	assert.Nil(t, err)
	assert.Equal(t, 200, respW.Code)
	assert.Equal(t, movie.Title, respDto.Title)
}

func TestDeleteMovie(t *testing.T) {
	respW := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", "/peliculas/10", nil)
	router.ServeHTTP(respW, req)

	jsonMessage := "{\"deleted\":\"10\"}"

	assert.Equal(t, 200, respW.Code)
	assert.Equal(t, jsonMessage, respW.Body.String())
}

func TestGetUsers(t *testing.T) {
	respW := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/users", nil)
	router.ServeHTTP(respW, req)

	var respDto []dto.UserDto
	err := json.Unmarshal(respW.Body.Bytes(), &respDto)

	assert.Nil(t, err)
	assert.Equal(t, 200, respW.Code)
	assert.Greater(t, len(respDto), 0)
}
