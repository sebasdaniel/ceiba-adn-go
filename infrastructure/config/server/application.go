package server

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/sebasdaniel/ceiba-adn-go/infrastructure/controller"
)

var router = gin.Default()

func StartApplication() {

	route()
	router.Run()
}

func route() {
	router.GET("/peliculas", controller.GetMovies)
	router.GET("/peliculas/:movie_id", controller.GetMovie)
	router.POST("/peliculas", controller.CreateMovie)
	router.PUT("/peliculas/:movie_id", controller.UpdateMovie)
	router.DELETE("/peliculas/:movie_id", controller.DeleteMovie)
	router.GET("/users", controller.GetUsers)
}
