package repository

import (
	"database/sql"
	"errors"
	"fmt"

	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model/dto"
)

const (
	SQL_SELECT_ALL_MOVIES                = "SELECT * FROM movie"
	SQL_INSERT_MOVIE                     = "INSERT INTO movie(title, year, genre, client_id) VALUES(?, ?, ?, ?)"
	SQL_UPDATE_MOVIE                     = "UPDATE movie SET title = ?, year = ?, genre = ? WHERE id = ?"
	SQL_DELETE_MOVIE                     = "DELETE FROM movie WHERE id = ?"
	SQL_FIND_MOVIE_BY_ID                 = "SELECT * FROM movie WHERE id = ?"
	SQL_EXISTS_MOVIE_BY_TITLE            = "SELECT COUNT(*) FROM movie WHERE title = ?"
	SQL_EXISTS_MOVIE_BY_TITLE_EXCLUDE_ID = "SELECT COUNT(*) FROM movie WHERE title = ? AND id != ?"
)

// MovieRepositoryMysqlImpl struct implementes the interface MovieRepository from the domain
// in this case the repository implementation is based on the mysql db engine (cause sql statments)
type MovieRepositoryMysqlImpl struct {
	dbHandler sql.DB
}

// NewMovieRepositoryMysqlImpl func represents the "constructor" of the MovieRepositoryMysqlImpl struct
func NewMovieRepositoryMysqlImpl(dbHandler *sql.DB) *MovieRepositoryMysqlImpl {
	return &MovieRepositoryMysqlImpl{
		dbHandler: *dbHandler,
	}
}

// FindAll gets all saved movies and return them in a slice
func (movieRepo *MovieRepositoryMysqlImpl) FindAll() (*[]dto.MovieDto, error) {
	preparedStatement, err := movieRepo.dbHandler.Prepare(SQL_SELECT_ALL_MOVIES)

	if err != nil {
		fmt.Sprintln("Error [FindAll]: Trying to prepare get all movies statement")
		return nil, err
	}

	defer preparedStatement.Close() // close prepared statement

	rows, err := preparedStatement.Query()

	if err != nil {
		fmt.Sprintln("Error [FindAll]: Trying to find all movies")
		return nil, err
	}

	defer rows.Close() // close rows

	var movies []dto.MovieDto

	for rows.Next() {
		var movie dto.MovieDto

		if err := rows.Scan(&movie.Id, &movie.Title, &movie.Year, &movie.Genre, &movie.ClientId); err != nil {
			fmt.Sprintln("Error [FindAll]: Scanning movies row")
			return nil, err
		}

		movies = append(movies, movie)
	}

	return &movies, nil
}

// FindById finds a movie by its id
func (movieRepo *MovieRepositoryMysqlImpl) FindById(id uint) (*dto.MovieDto, error) {
	preparedStatement, err := movieRepo.dbHandler.Prepare(SQL_FIND_MOVIE_BY_ID)

	if err != nil {
		fmt.Sprintln("Error [FindById]: Trying to prepare get all movies statement")
		return nil, err
	}

	defer preparedStatement.Close() // close prepared statement

	result := preparedStatement.QueryRow(id)

	var movieDto dto.MovieDto

	if getErr := result.Scan(&movieDto.Id, &movieDto.Title, &movieDto.Year, &movieDto.Genre, &movieDto.ClientId); getErr != nil {
		fmt.Sprintln("Error [FindById]: Scanning the movie row result")
		if errors.Is(getErr, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, getErr
	}

	return &movieDto, nil
}

// Create saves a movie to the database
func (movieRepo *MovieRepositoryMysqlImpl) Create(movie *model.Movie) (*dto.MovieDto, error) {
	preparedStatement, err := movieRepo.dbHandler.Prepare(SQL_INSERT_MOVIE)

	if err != nil {
		fmt.Sprintln("Error [Create]: Trying to prepare get all movies statement")
		return nil, err
	}

	defer preparedStatement.Close() // close prepared statement

	insertResult, saveErr := preparedStatement.Exec(
		movie.GetTitle(),
		movie.GetYear(),
		movie.GetGenre(),
		movie.GetClientId(),
	)

	if saveErr != nil {
		fmt.Sprintln("Error [Create]: Trying to save movie")
		return nil, saveErr
	}

	movieId, err := insertResult.LastInsertId()

	if err != nil {
		fmt.Sprintln("Error [Create]: Trying to get last inserted id")
		return nil, err
	}

	// find the saved movie
	findPreparedStmt, err := movieRepo.dbHandler.Prepare(SQL_FIND_MOVIE_BY_ID)

	if err != nil {
		fmt.Sprintln("Error [Create]: Trying to prepare get saved movie by id")
		return nil, err
	}

	defer findPreparedStmt.Close()

	result := findPreparedStmt.QueryRow(movieId)

	var movieDto dto.MovieDto

	if getErr := result.Scan(&movieDto.Id, &movieDto.Title, &movieDto.Year, &movieDto.Genre, &movieDto.ClientId); getErr != nil {
		fmt.Sprintln("Error [Create]: Trying to get the saved movie")
		return nil, getErr
	}

	return &movieDto, nil
}

// Update modifies the movie struct parameter representation to the database
func (movieRepo *MovieRepositoryMysqlImpl) Update(movie *model.Movie) (*dto.MovieDto, error) {
	preparedStatement, err := movieRepo.dbHandler.Prepare(SQL_UPDATE_MOVIE)

	if err != nil {
		fmt.Sprintln("Error [Update]: Trying to prepare update movie statement")
		return nil, err
	}

	defer preparedStatement.Close() // close prepared statement

	_, saveErr := preparedStatement.Exec(movie.GetTitle(), movie.GetYear(), movie.GetGenre(), movie.GetId())

	if saveErr != nil {
		fmt.Sprintln("Error [Update]: Trying to update the movie")
		return nil, saveErr
	}

	return &dto.MovieDto{
		Id:       movie.GetId(),
		Title:    movie.GetTitle(),
		Year:     movie.GetYear(),
		Genre:    movie.GetGenre(),
		ClientId: movie.GetClientId(),
	}, nil
}

// Delete remove the movie from the database (? soft deleting?)
func (movieRepo *MovieRepositoryMysqlImpl) Delete(id uint) error {

	preparedStatement, err := movieRepo.dbHandler.Prepare(SQL_DELETE_MOVIE)

	if err != nil {
		fmt.Sprintln("Error [Delete]: Trying to prepare delete movie statement")
		return err
	}

	defer preparedStatement.Close() // close prepared statement

	if _, saveErr := preparedStatement.Exec(id); saveErr != nil {
		fmt.Sprintln("Error [Delete]: Trying to delete the movie")
		return saveErr
	}

	return nil
}

// ExistsByTitle finds a movie by its title and says if it exists or not in the database
func (movieRepo *MovieRepositoryMysqlImpl) ExistsByTitle(title string) (bool, error) {
	preparedStatement, err := movieRepo.dbHandler.Prepare(SQL_EXISTS_MOVIE_BY_TITLE)

	if err != nil {
		fmt.Sprintln("Error [ExistsByTitle]: Trying to prepare exist by title statement")
		return false, err
	}

	defer preparedStatement.Close() // close prepared statement

	result := preparedStatement.QueryRow(title)

	var movieCount int

	if getErr := result.Scan(&movieCount); getErr != nil {
		fmt.Sprintln("Error [ExistsByTitle]: Scanning the movie count row result")
		return false, getErr
	}

	return (movieCount > 0), nil
}

// ExistsByTitleAndNotId finds a movie by its title but excluding the one that has its id, and
// returns if it exists or not in the database
func (movieRepo *MovieRepositoryMysqlImpl) ExistsByTitleAndNotId(title string, id uint) (bool, error) {
	preparedStatement, err := movieRepo.dbHandler.Prepare(SQL_EXISTS_MOVIE_BY_TITLE_EXCLUDE_ID)

	if err != nil {
		fmt.Sprintln("Error [ExistsByTitleAndNotId]: Trying to prepare exist by title excluding id statement")
		return false, err
	}

	defer preparedStatement.Close() // close prepared statement

	result := preparedStatement.QueryRow(title, id)

	var movieCount int

	if getErr := result.Scan(&movieCount); getErr != nil {
		fmt.Sprintln("Error [ExistsByTitleAndNotId]: Scanning the movie count row result")
		return false, getErr
	}

	return (movieCount > 0), nil
}
