package repository

import (
	"encoding/json"
	"fmt"
	"sync"

	"github.com/go-resty/resty/v2"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model/dto"
)

const SERVICE_URL = "https://reqres.in/api/users"

var lock = &sync.Mutex{}

var repoInstance *UserRepositoryRestImpl

type UserRepositoryRestImpl struct {
}

func GetUserRepoInstance() *UserRepositoryRestImpl {
	if repoInstance == nil {
		// lock execution against multithreading
		lock.Lock()
		defer lock.Unlock()
		// initialize the user repo instance
		repoInstance = &UserRepositoryRestImpl{}
		fmt.Println("User repository created!")
	}
	return repoInstance
}

func (ur *UserRepositoryRestImpl) FindAll() (*[]dto.UserDto, error) {
	restyClient := resty.New()

	resp, err := restyClient.R().
		SetQueryParam("page", "2").
		Get(SERVICE_URL)

	if err != nil {
		return nil, err
	}

	var reqresResponse struct {
		Data []dto.UserDto
	}

	// parse json response
	json.Unmarshal([]byte(resp.String()), &reqresResponse)

	return &reqresResponse.Data, nil
}
