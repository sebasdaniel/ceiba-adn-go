package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/sebasdaniel/ceiba-adn-go/application/command/manager"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/service"
	"gitlab.com/sebasdaniel/ceiba-adn-go/infrastructure/adapter/repository"
)

var (
	userRepository = repository.GetUserRepoInstance()

	readUserService = service.GetReadAllUsersServiceInstance(userRepository)

	readUserManager = manager.NewListUserManager(*readUserService)
)

func GetUsers(c *gin.Context) {
	users, err := readUserManager.Execute()

	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
	}

	c.JSON(http.StatusOK, users)
}
