package controller

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/sebasdaniel/ceiba-adn-go/application/command"
	"gitlab.com/sebasdaniel/ceiba-adn-go/application/command/factory"
	"gitlab.com/sebasdaniel/ceiba-adn-go/application/command/manager"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model/dto"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/service"
	"gitlab.com/sebasdaniel/ceiba-adn-go/infrastructure/adapter/repository"
	"gitlab.com/sebasdaniel/ceiba-adn-go/infrastructure/config/datasources/mysql/movies_db"
)

// TODO: init() y dejar aca solo las declaraciones generales, en init se crea la implementacion correspondiente
var (
	// BD handler (client)
	bdHandler = movies_db.DBHandler

	// movie repository (mysql)
	movieRepository = repository.NewMovieRepositoryMysqlImpl(bdHandler)

	// movie factory
	movieFactory = factory.MovieFactory{}

	// services
	readMoviesService  = service.GetReadAllMoviesServiceInstance(movieRepository)
	readMovieService   = service.GetReadMovieServiceInstance(movieRepository)
	createMovieService = service.GetCreateMovieServiceInstance(movieRepository)
	updateMovieService = service.GetUpdateMovieServiceInstance(movieRepository)
	deleteMovieService = service.GetDeleteMovieServiceInstance(movieRepository)

	// managers
	listMoviesManager  = manager.NewListMovieManager(*readMoviesService)
	getMovieManager    = manager.NewGetMovieManager(*readMovieService)
	createMovieManager = manager.NewCreateMovieManager(movieFactory, *createMovieService)
	updateMovieManager = manager.NewUpdateMovieManager(movieFactory, *updateMovieService)
	deleteMovieManager = manager.NewDeleteMovieManager(*deleteMovieService)
)

// Private Functions
func getMovieId(strId string) (uint64, error) {
	return strconv.ParseUint(strId, 10, 64)
}

// Controller Functions
func GetMovies(c *gin.Context) {
	movies, err := listMoviesManager.Execute()

	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	if movies == nil {
		movies = &[]dto.MovieDto{}
	}

	c.JSON(http.StatusOK, movies)
}

func GetMovie(c *gin.Context) {
	movieId, idErr := getMovieId(c.Param("movie_id"))
	//movieId, idErr := strconv.ParseUint(c.Param("movie_id"), 10, 64)

	if idErr != nil {
		c.JSON(http.StatusBadRequest, idErr.Error())
		return
	}

	movie, err := getMovieManager.Execute(uint(movieId))

	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	if movie == nil {
		c.JSON(http.StatusNotFound, "The movie you are searching was not found")
		return
	}

	c.JSON(http.StatusOK, movie)
}

func CreateMovie(c *gin.Context) {
	var movieCommand command.MovieCommand

	if err := c.ShouldBindJSON(&movieCommand); err != nil {
		c.JSON(http.StatusBadRequest, "Invalid JSON body")
		return
	}

	movie, err := createMovieManager.Execute(movieCommand)

	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusCreated, movie)
}

func UpdateMovie(c *gin.Context) {
	//movieId, idErr := strconv.ParseUint(c.Param("movie_id"), 10, 64)
	movieId, idErr := getMovieId(c.Param("movie_id"))

	if idErr != nil {
		c.JSON(http.StatusBadRequest, idErr.Error())
		return
	}

	var movieCommand command.MovieCommand

	if err := c.ShouldBindJSON(&movieCommand); err != nil {
		c.JSON(http.StatusBadRequest, "Invalid JSON body")
		return
	}

	movieCommand.Id = uint(movieId)

	movie, err := updateMovieManager.Execute(movieCommand)

	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, movie)
}

func DeleteMovie(c *gin.Context) {
	//movieId, idErr := strconv.ParseUint(c.Param("movie_id"), 10, 64)
	movieId, idErr := getMovieId(c.Param("movie_id"))

	if idErr != nil {
		c.JSON(http.StatusBadRequest, idErr.Error())
		return
	}

	if err := deleteMovieManager.Execute(uint(movieId)); err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, map[string]string{"deleted": fmt.Sprint(movieId)})
}
