package manager

import (
	"gitlab.com/sebasdaniel/ceiba-adn-go/application/command"
	"gitlab.com/sebasdaniel/ceiba-adn-go/application/command/factory"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model/dto"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/service"
)

type CreateMovieManager struct {
	movieFactory       factory.MovieFactory
	createMovieService service.CreateMovieService
}

func NewCreateMovieManager(moviefactory factory.MovieFactory, createMovieService service.CreateMovieService) *CreateMovieManager {
	return &CreateMovieManager{
		movieFactory:       moviefactory,
		createMovieService: createMovieService,
	}
}

func (cmm *CreateMovieManager) Execute(movieCommand command.MovieCommand) (*dto.MovieDto, error) {
	movie, err := cmm.movieFactory.Create(movieCommand)

	if err != nil {
		return nil, err
	}

	createdMovie, err := cmm.createMovieService.Execute(movie)

	if err != nil {
		return nil, err
	}

	return createdMovie, nil
}
