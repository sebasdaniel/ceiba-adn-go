package manager

import (
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model/dto"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/service"
)

type ListUserManager struct {
	readAllService service.ReadAllUsersService
}

func NewListUserManager(readAllService service.ReadAllUsersService) *ListUserManager {
	return &ListUserManager{
		readAllService: readAllService,
	}
}

func (lum *ListUserManager) Execute() (*[]dto.UserDto, error) {
	return lum.readAllService.Execute()
}
