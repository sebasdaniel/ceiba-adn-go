package manager

import (
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model/dto"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/service"
)

type ListMovieManager struct {
	readAllMS service.ReadAllMoviesService
}

func NewListMovieManager(readAllMS service.ReadAllMoviesService) *ListMovieManager {
	return &ListMovieManager{
		readAllMS: readAllMS,
	}
}

func (lmm *ListMovieManager) Execute() (*[]dto.MovieDto, error) {
	return lmm.readAllMS.Execute()
}
