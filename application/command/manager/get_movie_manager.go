package manager

import (
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model/dto"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/service"
)

type GetMovieManager struct {
	readMovieservice service.ReadMovieService
}

func NewGetMovieManager(readMovieService service.ReadMovieService) *GetMovieManager {
	return &GetMovieManager{
		readMovieservice: readMovieService,
	}
}

func (gmm *GetMovieManager) Execute(id uint) (*dto.MovieDto, error) {
	return gmm.readMovieservice.Execute(id)
}
