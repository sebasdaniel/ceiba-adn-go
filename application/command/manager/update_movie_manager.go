package manager

import (
	"gitlab.com/sebasdaniel/ceiba-adn-go/application/command"
	"gitlab.com/sebasdaniel/ceiba-adn-go/application/command/factory"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model/dto"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/service"
)

type UpdateMovieManager struct {
	movieFactory       factory.MovieFactory
	updateMovieService service.UpdateMovieService
}

func NewUpdateMovieManager(moviefactory factory.MovieFactory, updateMovieService service.UpdateMovieService) *UpdateMovieManager {
	return &UpdateMovieManager{
		movieFactory:       moviefactory,
		updateMovieService: updateMovieService,
	}
}

func (umm *UpdateMovieManager) Execute(movieCommand command.MovieCommand) (*dto.MovieDto, error) {
	movie, err := umm.movieFactory.Create(movieCommand)

	if err != nil {
		return nil, err
	}

	updatedMovie, err := umm.updateMovieService.Execute(movie)

	if err != nil {
		return nil, err
	}

	return updatedMovie, nil
}
