package manager

import (
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/service"
)

type DeleteMovieManager struct {
	deleteMovieService service.DeleteMovieService
}

func NewDeleteMovieManager(deleteMovieService service.DeleteMovieService) *DeleteMovieManager {
	return &DeleteMovieManager{
		deleteMovieService: deleteMovieService,
	}
}

func (dms *DeleteMovieManager) Execute(id uint) error {
	return dms.deleteMovieService.Execute(id)
}
