package command

type MovieCommand struct {
	Id       uint
	Title    string
	Year     uint16
	Genre    string
	ClientId int
}
