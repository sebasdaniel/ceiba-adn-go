package factory

import (
	"gitlab.com/sebasdaniel/ceiba-adn-go/application/command"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model"
)

type MovieFactory struct {
}

func (mf MovieFactory) Create(movieCommand command.MovieCommand) (*model.Movie, error) {
	movie, err := model.NewMovie(
		movieCommand.Id,
		movieCommand.Title,
		movieCommand.Year,
		movieCommand.Genre,
		movieCommand.ClientId,
	)

	return movie, err
}
