package factory

import (
	"gitlab.com/sebasdaniel/ceiba-adn-go/application/command"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model"
)

type UserFactory struct {
}

func (uf *UserFactory) Create(userCommand command.UserCommand) *model.User {
	return model.NewUser(
		userCommand.Id,
		userCommand.Email,
		userCommand.FirstName,
		userCommand.LastName,
		userCommand.Avatar,
	)
}
