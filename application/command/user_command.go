package command

type UserCommand struct {
	Id        int
	Email     string
	FirstName string
	LastName  string
	Avatar    string
}
