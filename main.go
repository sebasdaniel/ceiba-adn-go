package main

import (
	"gitlab.com/sebasdaniel/ceiba-adn-go/infrastructure/config/server"
)

func main() {
	server.StartApplication()
}
