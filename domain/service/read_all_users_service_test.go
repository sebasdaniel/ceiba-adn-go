package service

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model/dto"
)

// MockedUserRepository is a struct that represents (mock) an implementation
// of the UserRepository interface
type MockedUserRepository struct {
	mock.Mock
}

func (mock *MockedUserRepository) FindAll() (*[]dto.UserDto, error) {
	args := mock.Called()

	var users []dto.UserDto

	if rf, ok := args.Get(0).(func() ([]dto.UserDto, error)); ok {
		users, _ = rf()
	} else {
		users = args.Get(0).([]dto.UserDto)
	}

	return &users, nil
}

func TestReadAllUsers(t *testing.T) {
	// create MovieRepository from the mock
	userRepository := new(MockedUserRepository)

	// create the service with the mocked repository
	readAllUsersService := NewReadAllUsersService(userRepository)

	user1 := dto.UserDto{Id: 1, Email: "fulanito@example.com", FirstName: "Fulanito", LastName: "De Tal", Avatar: "fulanito.jpg"}
	user2 := dto.UserDto{Id: 2, Email: "sutanito@example.com", FirstName: "Sutanito", LastName: "De Tal", Avatar: "sutanito.jpg"}

	userList := []dto.UserDto{user1, user2}

	// mock the behavior of the "FindAll" method
	userRepository.On("FindAll").Return(userList)

	// act
	result, _ := readAllUsersService.Execute()

	assert.Equal(t, len(userList), len(*result))
}
