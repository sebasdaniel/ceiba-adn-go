package service

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model/dto"
)

func TestReadMovie(t *testing.T) {
	// create MovieRepository from the mock
	movieRepository := new(MockedMovieRepository)

	// create the service with the mocked repository
	readMovieService := GetReadMovieServiceInstance(movieRepository)

	movieToRead := dto.MovieDto{
		Id:    1,
		Title: "Alice in Wondeland",
		Year:  1990,
		Genre: "Fantasy",
	}

	// mock the behavior of the "FindById" method
	movieRepository.On("FindById", movieToRead.Id).Return(movieToRead)

	// act
	resultMovie, _ := readMovieService.Execute(movieToRead.Id)

	assert.Equal(t, movieToRead.Id, resultMovie.Id)
}
