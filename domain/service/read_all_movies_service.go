package service

import (
	"fmt"

	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model/dto"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/port"
)

var instanceRAMS *ReadAllMoviesService

type ReadAllMoviesService struct {
	movieRepository port.MovieRepository
}

func GetReadAllMoviesServiceInstance(movieRepository port.MovieRepository) *ReadAllMoviesService {
	if instanceRAMS == nil {
		// lock execution against multithreading
		lock.Lock()
		defer lock.Unlock()
		// initialize the user repo instance
		instanceRAMS = &ReadAllMoviesService{
			movieRepository: movieRepository,
		}
		fmt.Println("ReadAllMoviesService created!")
	}
	return instanceRAMS
}

func (rams *ReadAllMoviesService) Execute() (*[]dto.MovieDto, error) {
	return rams.movieRepository.FindAll()
}
