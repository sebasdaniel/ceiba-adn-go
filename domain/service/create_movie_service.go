package service

import (
	"fmt"
	"sync"

	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model/dto"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/port"
)

const MOVIE_EXISTS_MESSAGE = "The movie exists in the system"

var lock = &sync.Mutex{}

var instanceCMS *CreateMovieService

type CreateMovieService struct {
	movieRepository port.MovieRepository
}

func GetCreateMovieServiceInstance(movieRepository port.MovieRepository) *CreateMovieService {
	if instanceCMS == nil {
		// lock execution against multithreading
		lock.Lock()
		defer lock.Unlock()
		// initialize the user repo instance
		instanceCMS = &CreateMovieService{
			movieRepository: movieRepository,
		}
		fmt.Println("CreateMovieService created!")
	}
	return instanceCMS
}

func (cms *CreateMovieService) Execute(movie *model.Movie) (*dto.MovieDto, error) {
	err := cms.validMovie(movie)

	if err != nil {
		return nil, err
	}

	return cms.movieRepository.Create(movie)
}

func (cms *CreateMovieService) validMovie(movie *model.Movie) error {
	exist, err := cms.movieRepository.ExistsByTitle(movie.GetTitle())

	if err != nil {
		return err
	}

	if exist {
		return fmt.Errorf(MOVIE_EXISTS_MESSAGE)
	}

	return nil
}
