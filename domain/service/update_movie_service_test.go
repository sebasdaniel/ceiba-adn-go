package service

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model/dto"
)

// TestUpdateMovie_MovieExistsError is a function that tests
// the movie update service with an existent movie title
func TestUpdateMovie_MovieExistsError(t *testing.T) {
	// create MovieRepository from the mock
	movieRepository := new(MockedMovieRepository)

	// create the service with the mocked repository
	updateMovieService := GetUpdateMovieServiceInstance(movieRepository)

	movieToUpdate, _ := model.NewMovie(1, "Terminator", 1990, "Action", 1)

	// mock the behavior of the "ExistsByTitleAndNotId" method
	movieRepository.On("ExistsByTitleAndNotId", movieToUpdate.GetTitle(), movieToUpdate.GetId()).Return(true)

	// act
	resultMovie, err := updateMovieService.Execute(movieToUpdate)

	// assert
	assert.Error(t, err, "Not an error")
	assert.Nil(t, resultMovie, "Movie is not nil")
}

// TestUpdateMovie_HappyCase is a function that tests
// the movie update service with valid updated movie
func TestUpdateMovie_HappyCase(t *testing.T) {
	// create MovieRepository from the mock
	movieRepository := new(MockedMovieRepository)

	updateMovieService := GetUpdateMovieServiceInstance(movieRepository)

	movieToUpdate, _ := model.NewMovie(1, "Terminator", 1990, "Action", 1)
	movieUpdated := dto.MovieDto{
		Id:       1,
		Title:    "Terminator",
		Year:     1990,
		Genre:    "Action",
		ClientId: 1,
	}

	movieRepository.On("ExistsByTitleAndNotId", movieToUpdate.GetTitle(), movieToUpdate.GetId()).Return(false)
	movieRepository.On("Update", movieToUpdate).Return(movieUpdated)

	// act
	resultMovie, err := updateMovieService.Execute(movieToUpdate)

	// assert
	assert.Equal(t, movieToUpdate.GetId(), resultMovie.Id, "Movies Id are different")
	assert.Nil(t, err, "Error not nil")
}
