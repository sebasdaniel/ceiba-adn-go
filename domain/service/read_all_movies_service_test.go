package service

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model/dto"
)

func TestReadAllMovies(t *testing.T) {
	// create MovieRepository from the mock
	movieRepository := new(MockedMovieRepository)

	// create the service with the mocked repository
	readAllMovieService := GetReadAllMoviesServiceInstance(movieRepository)

	movie1 := dto.MovieDto{Id: 1, Title: "Alice in Wonderland", Year: 1990, Genre: "Fantasy"}
	movie2 := dto.MovieDto{Id: 2, Title: "Terminator", Year: 1990, Genre: "Action"}
	movie3 := dto.MovieDto{Id: 3, Title: "Avengers", Year: 200, Genre: "Adventure"}
	movie4 := dto.MovieDto{Id: 4, Title: "Friday the 13th", Year: 1980, Genre: "Thriller"}

	movieList := []dto.MovieDto{movie1, movie2, movie3, movie4}

	// mock the behavior of the "FindAll" method
	movieRepository.On("FindAll").Return(movieList)

	// act
	result, _ := readAllMovieService.Execute()

	assert.Equal(t, len(movieList), len(*result))
}
