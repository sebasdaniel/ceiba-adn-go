package service

import (
	"fmt"

	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model/dto"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/port"
)

var instanceRAUS *ReadAllUsersService

type ReadAllUsersService struct {
	userRepository port.UserRepository
}

func GetReadAllUsersServiceInstance(userRepository port.UserRepository) *ReadAllUsersService {
	if instanceRAUS == nil {
		// lock process
		lock.Lock()
		defer lock.Unlock()

		// create instance
		instanceRAUS = &ReadAllUsersService{
			userRepository: userRepository,
		}

		fmt.Println("ReadAllUsersService created!")
	}

	return instanceRAUS
}

func (raus *ReadAllUsersService) Execute() (*[]dto.UserDto, error) {
	return raus.userRepository.FindAll()
}
