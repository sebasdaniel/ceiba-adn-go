package service

import (
	"fmt"

	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model/dto"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/port"
)

var instanceRMS *ReadMovieService

type ReadMovieService struct {
	movieRepository port.MovieRepository
}

func GetReadMovieServiceInstance(movieRepository port.MovieRepository) *ReadMovieService {
	if instanceRMS == nil {
		// lock execution against multithreading
		lock.Lock()
		defer lock.Unlock()
		// initialize the user repo instance
		instanceRMS = &ReadMovieService{
			movieRepository: movieRepository,
		}
		fmt.Println("ReadMovieService created!")
	}
	return instanceRMS
}

func (rms *ReadMovieService) Execute(id uint) (*dto.MovieDto, error) {
	return rms.movieRepository.FindById(id)
}
