package service

import (
	"fmt"

	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model/dto"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/port"
)

var instanceUMS *UpdateMovieService

type UpdateMovieService struct {
	movieRepository port.MovieRepository
}

func GetUpdateMovieServiceInstance(movieRepository port.MovieRepository) *UpdateMovieService {
	if instanceUMS == nil {
		// lock execution against multithreading
		lock.Lock()
		defer lock.Unlock()
		// initialize the user repo instance
		instanceUMS = &UpdateMovieService{
			movieRepository: movieRepository,
		}
		fmt.Println("UpdateMovieService created!")
	}
	return instanceUMS
}

func (ums *UpdateMovieService) Execute(movie *model.Movie) (*dto.MovieDto, error) {
	err := ums.validMovieTitle(movie)

	if err != nil {
		return nil, err
	}

	return ums.movieRepository.Update(movie)
}

func (ums *UpdateMovieService) validMovieTitle(movie *model.Movie) error {
	exist, err := ums.movieRepository.ExistsByTitleAndNotId(movie.GetTitle(), movie.GetId())

	if err != nil {
		return err
	}

	if exist {
		return fmt.Errorf(MOVIE_EXISTS_MESSAGE)
	}

	return nil
}
