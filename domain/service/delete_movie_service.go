package service

import (
	"fmt"

	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/port"
)

type DeleteMovieService struct {
	movieRepository port.MovieRepository
}

var instanceDMS *DeleteMovieService

func GetDeleteMovieServiceInstance(movieRepository port.MovieRepository) *DeleteMovieService {
	if instanceDMS == nil {
		// lock execution against multithreading
		lock.Lock()
		defer lock.Unlock()
		// initialize the user repo instance
		instanceDMS = &DeleteMovieService{
			movieRepository: movieRepository,
		}
		fmt.Println("DeleteMovieService created!")
	}
	return instanceDMS
}

func (dms *DeleteMovieService) Execute(id uint) error {
	return dms.movieRepository.Delete(id)
}
