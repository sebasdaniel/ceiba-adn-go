package service

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model/dto"
)

// MockedMovieRepository is a struct that represents (mock) an implementation
// of the MovieRepository interface
type MockedMovieRepository struct {
	mock.Mock
}

func (mock *MockedMovieRepository) FindAll() (*[]dto.MovieDto, error) {
	args := mock.Called()

	var movies []dto.MovieDto

	if rf, ok := args.Get(0).(func() ([]dto.MovieDto, error)); ok {
		movies, _ = rf()
	} else {
		movies = args.Get(0).([]dto.MovieDto)
	}

	return &movies, nil
}

func (mock *MockedMovieRepository) FindById(id uint) (*dto.MovieDto, error) {
	args := mock.Called(id)

	var movie dto.MovieDto

	if rf, ok := args.Get(0).(func(uint) (dto.MovieDto, error)); ok {
		movie, _ = rf(id)
	} else {
		movie = args.Get(0).(dto.MovieDto)
	}

	return &movie, nil
}

func (mock *MockedMovieRepository) Create(movie *model.Movie) (*dto.MovieDto, error) {
	args := mock.Called(movie)

	var newMovie dto.MovieDto

	if rf, ok := args.Get(0).(func(model.Movie) (dto.MovieDto, error)); ok {
		newMovie, _ = rf(*movie)
	} else {
		newMovie = args.Get(0).(dto.MovieDto)
	}

	return &newMovie, nil
}

func (mock *MockedMovieRepository) Update(movie *model.Movie) (*dto.MovieDto, error) {
	args := mock.Called(movie)

	var newMovie dto.MovieDto

	if rf, ok := args.Get(0).(func(model.Movie) (dto.MovieDto, error)); ok {
		newMovie, _ = rf(*movie)
	} else {
		newMovie = args.Get(0).(dto.MovieDto)
	}

	return &newMovie, nil
}

func (mock *MockedMovieRepository) Delete(id uint) error {
	//TODO
	return nil
}

func (mock *MockedMovieRepository) ExistsByTitle(title string) (bool, error) {
	args := mock.Called(title)
	return args.Bool(0), nil
}

func (mock *MockedMovieRepository) ExistsByTitleAndNotId(title string, id uint) (bool, error) {
	args := mock.Called(title, id)
	return args.Bool(0), nil
}

// TestCreateMovie_MovieExistsError is a function that tests
// the movie creation service with an existent movie
func TestCreateMovie_MovieExistsError(t *testing.T) {
	// create MovieRepository from the mock
	movieRepository := new(MockedMovieRepository)

	// create the service with the mocked repository
	createMovieService := GetCreateMovieServiceInstance(movieRepository)

	movieToCreate, _ := model.NewMovie(1, "Terminator", 1990, "Action", 1)

	// mock the behavior of the "ExistsByTitle" method
	movieRepository.On("ExistsByTitle", "Terminator").Return(true)

	// act
	resultMovie, err := createMovieService.Execute(movieToCreate)

	// assert
	assert.Error(t, err, "Not an error")
	assert.Nil(t, resultMovie, "Movie not nil")
}

// TestCreateMovie_HappyCase is a function that tests
// the movie creation service with new movie
func TestCreateMovie_HappyCase(t *testing.T) {
	// create MovieRepository from the mock
	movieRepository := new(MockedMovieRepository)

	createMovieService := GetCreateMovieServiceInstance(movieRepository)

	movieToCreate, _ := model.NewMovie(0, "Terminator", 1990, "Action", 1)
	expectedMovie := dto.MovieDto{
		Id:       1,
		Title:    "Terminator",
		Year:     1990,
		Genre:    "Action",
		ClientId: 1,
	}

	movieRepository.On("ExistsByTitle", "Terminator").Return(false)
	movieRepository.On("Create", movieToCreate).Return(expectedMovie)

	// act
	resultMovie, err := createMovieService.Execute(movieToCreate)

	// assert
	assert.Equal(t, expectedMovie.Id, resultMovie.Id, "Movies Id are different")
	assert.Nil(t, err, "Error not nil")
}
