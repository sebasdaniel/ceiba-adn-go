package dto

type MovieDto struct {
	Id       uint
	Title    string
	Year     uint16
	Genre    string
	ClientId int
}
