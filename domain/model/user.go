package model

type User struct {
	id        int
	email     string
	firstName string
	lastName  string
	avatar    string
}

func NewUser(id int, email string, firstName string, lastName string, avatar string) *User {
	return &User{
		id:        id,
		email:     email,
		firstName: firstName,
		lastName:  lastName,
		avatar:    avatar,
	}
}

func (u *User) GetId() int {
	return u.id
}

func (u *User) GetEmail() string {
	return u.email
}

func (u *User) GetFirstName() string {
	return u.firstName
}

func (u *User) GetLastName() string {
	return u.lastName
}

func (u *User) GetAvatar() string {
	return u.avatar
}
