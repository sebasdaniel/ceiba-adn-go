package model

import "fmt"

const (
	MUST_ENTER_TITLE  = "Title must be present"
	MUST_ENTER_YEAR   = "Year must be present"
	MUST_ENTER_CLIENT = "Client Id must be present"
)

type Movie struct {
	id       uint
	title    string
	year     uint16
	genre    string
	clientId int
}

func NewMovie(id uint, title string, year uint16, genre string, clientId int) (*Movie, error) {
	if len(title) == 0 {
		return nil, fmt.Errorf(MUST_ENTER_TITLE)
	}

	if year == 0 {
		return nil, fmt.Errorf(MUST_ENTER_YEAR)
	}

	if clientId == 0 {
		return nil, fmt.Errorf(MUST_ENTER_CLIENT)
	}

	return &Movie{
		id:       id,
		title:    title,
		year:     year,
		genre:    genre,
		clientId: clientId,
	}, nil
}

func (m *Movie) GetId() uint {
	return m.id
}

func (m *Movie) GetTitle() string {
	return m.title
}

func (m *Movie) GetYear() uint16 {
	return m.year
}

func (m *Movie) GetGenre() string {
	return m.genre
}

func (m *Movie) GetClientId() int {
	return m.clientId
}
