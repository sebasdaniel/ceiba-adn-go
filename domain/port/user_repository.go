package port

import "gitlab.com/sebasdaniel/ceiba-adn-go/domain/model/dto"

type UserRepository interface {
	FindAll() (*[]dto.UserDto, error)
}
