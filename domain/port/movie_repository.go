package port

import (
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model"
	"gitlab.com/sebasdaniel/ceiba-adn-go/domain/model/dto"
)

type MovieRepository interface {
	FindAll() (*[]dto.MovieDto, error)

	FindById(id uint) (*dto.MovieDto, error)

	Create(movie *model.Movie) (*dto.MovieDto, error)

	Update(movie *model.Movie) (*dto.MovieDto, error)

	Delete(id uint) error

	ExistsByTitle(title string) (bool, error)

	ExistsByTitleAndNotId(title string, id uint) (bool, error)
}
